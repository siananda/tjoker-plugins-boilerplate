<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 */
// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit;
}




