<?php
/**
 * The ajax-facing functionality of the plugin.
 *
 * @since      1.0.0
 * @link       http://saifulananda.me/
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * The ajax-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and hooks for ajax
 * specific functionality of the plugin.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */

if( ! class_exists( 'TJoker_Plugins_Boilerplate_Ajax' ) ) :
	class TJoker_Plugins_Boilerplate_Ajax {
		/**
		 * The ID of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $plugin_name    The ID of this plugin.
		 */
		private static $plugin_name = TJOKERPB_NAME;

		/**
		 * The version of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $version    The current version of this plugin.
		 */
		private static $version = TJOKERPBVERSION;

		/**
		 * @var null
		 */
		protected static $_instance = null;

		/**
		 * Instantiate Class
		 */
		public static function instance( $loader ) {

			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self( $loader );
			}
			return self::$_instance;
		}

		/**
		 * Register all of the hooks related to the Ajax area functionality
		 * of the plugin.
		 *
		 * @since    1.0.0
		 * @param    object    $loader       The Hook Loader Class Object of this plugin.
		 */
		public function __construct( $loader ) {

			$loader->add_action( 'wp_enqueue_scripts', $this, 'tjoker_public_ajax_scripts' );
			$loader->add_action('admin_enqueue_scripts', $this, 'tjoker_admin_ajax_scripts');
			// $loader->add_action( 'wp_ajax_tj_ajax_function', $this, 'tj_ajax_function' );
			// $loader->add_action( 'wp_ajax_nopriv_tj_ajax_function', $this, 'tj_ajax_function' );
		}

		/**
		 * Register the Ajax JavaScript for the admin area.
		 *
		 * @since    1.0.0
		 */
		public function tjoker_admin_ajax_scripts() {

			wp_enqueue_script( self::$plugin_name . 'admin-ajax', TJOKERPB_PLUGINS_DIR_URI . 'assets/js/tjoker-plugins-boilerplate-admin-ajax.js', array('jquery'), self::$version, false );
		}

		/**
		 * Register the ajax JavaScript for the public area.
		 *
		 * @since    1.0.0
		 */
		public function tjoker_public_ajax_scripts() {

			wp_enqueue_script( self::$plugin_name . 'public-ajax', TJOKERPB_PLUGINS_DIR_URI . 'assets/js/tjoker-plugins-boilerplate-public-ajax.js', array('jquery'), self::$version, false );
		}
	}
endif;