<?php
/**
 * Fired during plugin activation.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */

if( ! class_exists( 'TJoker_Plugins_Boilerplate_Activator' ) ) :
	class TJoker_Plugins_Boilerplate_Activator {
		/**
		 * Short Description. (use period)
		 *
		 * Long Description.
		 *
		 * @since    1.0.0
		 */
		public static function activate() {

		}
	}
endif;
