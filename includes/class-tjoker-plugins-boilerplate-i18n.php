<?php
/**
 * File which has the class that defines the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */

if( ! class_exists( 'TJoker_Plugins_Boilerplate_i18n' ) ) :
	class TJoker_Plugins_Boilerplate_i18n {
		/**
		 * The domain specified for this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $domain    The domain identifier for this plugin.
		 */
		private $domain;

		/**
		 * Load the plugin text domain for translation.
		 *
		 * @since    1.0.0
		 */
		public function tjoker_load_plugin_textdomain() {

			load_plugin_textdomain(
				$this->domain,
				false,
				dirname( dirname( plugin_basename( __FILE__ ) ) ) . 'languages/'
			);
		}

		/**
		 * Set the domain equal to that of the specified domain.
		 *
		 * @since    1.0.0
		 * @param    string    $domain    The domain that represents the locale of this plugin.
		 */
		public function set_domain( $domain ) {

			$this->domain = $domain;
		}
	}
endif;