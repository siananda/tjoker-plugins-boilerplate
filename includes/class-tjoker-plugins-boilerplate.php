<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @since      1.0.0
 * @link       http://saifulananda.me/
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @link       http://saifulananda.me/
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */

if( ! class_exists( 'TJoker_Plugins_Boilerplate' ) ) :
	class TJoker_Plugins_Boilerplate {
		/**
		 * The loader that's responsible for maintaining and registering all hooks that power
		 * the plugin.
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      object    $loader    Maintains and registers all hooks for the plugin.
		 */
		protected static $loader;

		/**
		 * The unique identifier of this plugin.
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
		 */
		protected static $plugin_name;

		/**
		 * The current version of the plugin.
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $version    The current version of the plugin.
		 */
		protected static $version;

		/**
		 * Define the core functionality of the plugin.
		 *
		 * Set the plugin name and the plugin version that can be used throughout the plugin.
		 * Load the dependencies, define the locale, and set the hooks for the admin area and
		 * the public-facing side of the site.
		 *
		 * @since    1.1.0
		 */
		public function __construct() {

			self::$plugin_name = TJOKERPB_NAME;
			self::$version = TJOKERPBVERSION;

			$this->tjoker_load_dependencies();
			$this->tjoker_set_locale();
			$this->tjoker_hooks_define();
		}

		/**
		 * Load the required dependencies for this plugin.
		 *
		 * Include the following files that make up the plugin:
		 *
		 * - TJoker_Plugins_Boilerplate_Loader. Orchestrates the hooks of the plugin.
		 * - TJoker_Plugins_Boilerplate_i18n. Defines internationalization functionality.
		 * - TJoker_Plugins_Boilerplate_Admin. Defines all hooks for the admin area.
		 * - TJoker_Plugins_Boilerplate_Public. Defines all hooks for the public side of the site.
		 * - TJoker_Plugins_Boilerplate_Ajax. Defines all hooks for the ajax functionality of the site.
		 *
		 * Create an instance of the loader which will be used to register the hooks
		 * with WordPress.
		 *
		 * @since    1.0.0
		 * @access   private
		 */
		private function tjoker_load_dependencies() {

			$file_paths = array(
					'vendor/tjokerframework/tjokerframework.php',
					'partials/helper.php',
					'partials/setting_panel.php',
					'includes/class-tjoker-plugins-boilerplate-loader.php',
					'includes/class-tjoker-plugins-boilerplate-i18n.php',
					'includes/class-tjoker-plugins-boilerplate-admin.php',
					'includes/class-tjoker-plugins-boilerplate-public.php',
					'includes/class-tjoker-plugins-boilerplate-ajax.php',
				);

			/**
			 * Load require file for this plugins.
			 */
			foreach ( $file_paths as $path ) :
				$file_full_path = TJOKERPB_PLUGINS_DIR . $path;
				if ( file_exists( $file_full_path ) ):
					require_once $file_full_path;
				endif;
			endforeach;

			self::$loader = new TJoker_Plugins_Boilerplate_Loader();
			//AJAX hooks
			TJoker_Plugins_Boilerplate_Ajax::instance( self::tjoker_get_loader() );
			$settings = new TJoker_Setting_Controls();


		}

		/**
		 * Define the locale for this plugin for internationalization.
		 *
		 * Uses the TJoker_Plugins_Boilerplate_i18n class in order to set the domain and to register the hook
		 * with WordPress.
		 *
		 * @since    1.0.0
		 * @access   private
		 */
		private function tjoker_set_locale() {

			$tjoker_i18n = new TJoker_Plugins_Boilerplate_i18n();
			$tjoker_i18n->set_domain( self::tjoker_get_plugin_name() );

			self::$loader->add_action('plugins_loaded', $tjoker_i18n, 'tjoker_load_plugin_textdomain');
		}

		/**
		 * Register all of the hooks related to the ICO Theme functionality
		 *
		 * @since    1.0.0
		 * @access   private
		 */
		private function tjoker_hooks_define() {

			// Initialize admin class.
			$tjoker_admin   = new TJoker_Plugins_Boilerplate_Admin( self::tjoker_get_loader() );
			// Initialize public class.
			$tjoker_public  = new TJoker_Plugins_Boilerplate_Public( self::tjoker_get_loader() );
		}

		/**
		 * Run the loader to execute all of the hooks with WordPress.
		 *
		 * @since    1.0.0
		 */
		public function run() {

			self::$loader->run();

		}

		/**
		 * The name of the plugin used to uniquely identify it within the context of
		 * WordPress and to define internationalization functionality.
		 *
		 * @since     1.0.0
		 * @return    string    The name of the plugin.
		 */
		public static function tjoker_get_plugin_name() {

			return self::$plugin_name;

		}

		/**
		 * The reference to the class that orchestrates the hooks with the plugin.
		 *
		 * @since     1.0.0
		 * @return    TJoker_Plugins_Boilerplate_Loader    Orchestrates the hooks of the plugin.
		 */
		public static function tjoker_get_loader() {

			return self::$loader;

		}

		/**
		 * Retrieve the version number of the plugin.
		 *
		 * @since     1.0.0
		 * @return    string    The version number of the plugin.
		 */
		public static function tjoker_get_version() {

			return self::$version;

		}
	}
endif;
