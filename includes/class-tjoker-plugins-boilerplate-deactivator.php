<?php
/**
 * Fired during plugin deactivation.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */


if( ! class_exists( 'TJoker_Plugins_Boilerplate_Deactivator' ) ) :
	class TJoker_Plugins_Boilerplate_Deactivator {
		/**
		 * Short Description. (use period)
		 *
		 * Long Description.
		 *
		 * @since    1.0.0
		 */
		public static function deactivate(){

		}
	}
endif;