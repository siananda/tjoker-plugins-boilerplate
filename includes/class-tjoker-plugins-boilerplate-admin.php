<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and hooks for admin
 * specific functionality of the plugin.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */


if( ! class_exists( 'TJoker_Plugins_Boilerplate_Admin' ) ) :
	class TJoker_Plugins_Boilerplate_Admin {
		/**
		 * The ID of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $plugin_name    The ID of this plugin.
		 */
		private static $plugin_name = TJOKERPB_NAME;

		/**
		 * The version of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $version    The current version of this plugin.
		 */
		private static $version = TJOKERPBVERSION;

		/**
		 * Register all of the hooks related to the admin area functionality
		 * of the plugin.
		 *
		 * @since    1.0.0
		 * @param    object    $loader       The Hook Loader Class Object of this plugin.
		 */
		public function __construct( $loader ) {

			$loader->add_action('admin_enqueue_scripts', $this, 'tjoker_enqueue_styles');
			$loader->add_action('admin_enqueue_scripts', $this, 'tjoker_enqueue_scripts');
		}

		/**
		 * Register the stylesheets for the admin area.
		 *
		 * @since    1.0.0
		 */
		public function tjoker_enqueue_styles() {

			wp_enqueue_style( self::$plugin_name . 'admin-css', TJOKERPB_PLUGINS_DIR_URI . 'assets/css/tjoker-plugins-boilerplate-admin.css', array(), self::$version, 'all' );
		}

		/**
		 * Register the JavaScript for the admin area.
		 *
		 * @since    1.0.0
		 */
		public function tjoker_enqueue_scripts() {

			wp_enqueue_script( self::$plugin_name . 'admin-js', TJOKERPB_PLUGINS_DIR_URI . 'assets/js/tjoker-plugins-boilerplate-admin.js', array('jquery'), self::$version, false );
		}
	}
endif;