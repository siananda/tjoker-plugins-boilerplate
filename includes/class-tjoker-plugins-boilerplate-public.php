<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 * @link       http://saifulananda.me/
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/public
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * The public-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and hooks for public
 * specific functionality of the plugin.
 *
 * @link       http://saifulananda.me/
 * @since      1.0.0
 *
 * @package    TJoker_Plugins_Boilerplate
 * @subpackage TJoker_Plugins_Boilerplate/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */

if( ! class_exists( 'TJoker_Plugins_Boilerplate_Public' ) ) :
	class TJoker_Plugins_Boilerplate_Public {
		/**
		 * The ID of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $plugin_name    The ID of this plugin.
		 */
		private static $plugin_name = TJOKERPB_NAME;

		/**
		 * The version of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $version    The current version of this plugin.
		 */
		private static $version = TJOKERPBVERSION;

		/**
		 * Register all of the hooks related to the Public area functionality
		 * of the plugin.
		 *
		 * @since    1.0.0
		 * @param    object    $loader       The Hook Loader Class Object of this plugin.
		 */
		public function __construct( $loader ) {

			$loader->add_action( 'wp_enqueue_scripts', $this, 'tjoker_enqueue_styles' );
			$loader->add_action( 'wp_enqueue_scripts', $this, 'tjoker_enqueue_scripts' );
		}

		/**
		 * Register the stylesheets for the public-facing side of the site.
		 *
		 * @since    1.0.0
		 */
		public function tjoker_enqueue_styles() {

			wp_enqueue_style( self::$plugin_name . 'public-css', TJOKERPB_PLUGINS_DIR_URI . 'assets/css/tjoker-plugins-boilerplate-public.css', array(), self::$version, 'all');
		}

		/**
		 * Register the stylesheets for the public-facing side of the site.
		 *
		 * @since    1.0.0
		 */
		public function tjoker_enqueue_scripts() {

			wp_enqueue_script( self::$plugin_name . 'public-js', TJOKERPB_PLUGINS_DIR_URI . 'assets/js/tjoker-plugins-boilerplate-public.js', array('jquery'), self::$version, false);
		}
	}
endif;