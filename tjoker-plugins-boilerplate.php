<?php
/**
 * Plugin Name:       TJoker Plugins Boilerplate.
 * Plugin URI:        http://themejoker.com/
 * Description:       TJoker plugins boilerplate for wordpress plugins.
 * Version:           2.0.0
 * Author:            Saiful Islam Ananda
 * Author URI:        http://saifulananda.me/
 * License:           GNU General Public License v2 or later
 * Text Domain:       tjoker-plugins-boilerplate
 * Domain Path:       /languages
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) :
	exit;
endif;

/**
 * Stores plugin file path.
 */
define( 'TJOKERPB', __FILE__ );
/**
 * Plugin name in slug.
 */
define( 'TJOKERPB_NAME', 'tjoker-plugins-boilerplate' );
/**
 * Plugin version.
 */
define( 'TJOKERPBVERSION', '1.0.0' );
/**
 * Plugin directory.
 */
define( 'TJOKERPB_PLUGINS_DIR', trailingslashit( plugin_dir_path( TJOKERPB ) ) );
/**
 * Plugin directory url.
 */
define( 'TJOKERPB_PLUGINS_DIR_URI', trailingslashit( plugin_dir_url( TJOKERPB ) ) );

/**
 * Function that runs during plugin activation.
 */
function tjoker_plugins_boilerplate_activate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tjoker-plugins-boilerplate-activator.php';
	TJoker_Plugins_Boilerplate_Activator::activate();
}

/**
 * Function that runs during plugin deactivation.
 */
function tjoker_plugins_boilerplate_deactivate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tjoker-plugins-boilerplate-deactivator.php';
	TJoker_Plugins_Boilerplate_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'tjoker_plugins_boilerplate_activate' );
register_deactivation_hook( __FILE__, 'tjoker_plugins_boilerplate_deactivate' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tjoker-plugins-boilerplate.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_tjoker_plugins_boilerplate() {
	$plugin = new TJoker_Plugins_Boilerplate();
	$plugin->run();
}

add_action( 'plugins_loaded', 'run_tjoker_plugins_boilerplate' );