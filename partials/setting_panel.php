<?php
/**
 * Tjoker Example Settings Controls
 *
 * @version 1.0
 *
 */
if ( ! class_exists( 'TJoker_Setting_Controls' ) ):
class TJoker_Setting_Controls {

	private $settings_api;

	function __construct() {

		$this->settings_api = new TJoker_Setting_API;

		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
	}

	function admin_init() {

		//set the settings
		$this->settings_api->set_sections( $this->get_settings_sections() );
		$this->settings_api->set_fields( $this->get_settings_fields() );

		//initialize settings
		$this->settings_api->admin_init();
	}

	function admin_menu() {
		add_options_page( 'Event Settings', 'Event Settings', 'delete_posts', 'mep_event_settings_page', array($this, 'plugin_page') );
	}

	function get_settings_sections() {

		$sections = array(
			array(
				'id' => 'general_setting_sec',
				'title' => __( 'General Settings', 'tjoker-fbwc-integration' )
			),
			array(
				'id' => 'email_setting_sec',
				'title' => __( 'Email Settings', 'tjoker-fbwc-integration' )
			),
			array(
				'id' => 'style_setting_sec',
				'title' => __( 'Style Settings', 'tjoker-fbwc-integration' )
			),
			array(
				'id' => 'label_setting_sec',
				'title' => __( 'Label Settings', 'tjoker-fbwc-integration' )
			)
		);



		return $sections;
	}

	/**
	 * Returns all the settings fields
	 *
	 * @return array settings fields
	 */
	function get_settings_fields() {
		$settings_fields = array(
			'general_setting_sec' => array(
				array(
					'name' => 'google-map-api',
					'label' => __( 'Google Map API Key', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Enter Your Google Map API key. <a href=https://developers.google.com/maps/documentation/javascript/get-api-key target=_blank>Get KEY</a>', 'tjoker-fbwc-integration' ),
					'type' => 'text',
					'default' => ''
				),
				array(
					'name' => 'mep_global_single_template',
					'label' => __( 'Event Details Template', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Event Details Template', 'tjoker-fbwc-integration' ),
					'type' => 'select',
					'default' => 'no',
					//'options' =>  event_template_name()
				),
			),

			'email_setting_sec' => array(
				array(
					'name' => 'mep_email_form_name',
					'label' => __( 'Email Form Name', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Email Form Name', 'tjoker-fbwc-integration' ),
					'type' => 'text'
				),
				array(
					'name' => 'mep_email_form_email',
					'label' => __( 'Form Email', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Form Email', 'tjoker-fbwc-integration' ),
					'type' => 'text'
				),
				array(
					'name' => 'mep_email_subject',
					'label' => __( 'Email Subject', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Email Subject', 'tjoker-fbwc-integration' ),
					'type' => 'text'
				),
				array(
					'name' => 'mep_confirmation_email_text',
					'label' => __( 'Confirmation Email Text', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Confirmation Email Text', 'tjoker-fbwc-integration' ),
					'type' => 'textarea',
					'default' => '',
				),
			),

			'label_setting_sec' => array(
				array(
					'name' => 'mep_event_ticket_type_text',
					'label' => __( 'Ticket Type Table Label', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Enter the text which you want to display as ticket type table in event details page.', 'tjoker-fbwc-integration' ),
					'type' => 'text',
					'default' => 'Ticket Type:'
				),
				array(
					'name' => 'mep_event_extra_service_text',
					'label' => __( 'Extra Service Table Label', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Enter the text which you want to display as extra service table in event details page.', 'tjoker-fbwc-integration' ),
					'type' => 'text',
					'default' => 'Extra Service:'
				),
				array(
					'name' => 'mep_cart_btn_text',
					'label' => __( 'Cart Button Label', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Enter the text which you want to display in Cart button in event details page.', 'tjoker-fbwc-integration' ),
					'type' => 'text',
					'default' => 'Register This Event'
				),
				array(
					'name' => 'mep_calender_btn_text',
					'label' => __( 'Add Calender Button Label', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Enter the text which you want to display in Add you calender in event details page.', 'tjoker-fbwc-integration' ),
					'type' => 'text',
					'default' => 'ADD TO YOUR CALENDAR'
				),
				array(
					'name' => 'mep_share_text',
					'label' => __( 'Social Share Label', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Enter the text which you want to display as share button title in event details page.', 'tjoker-fbwc-integration' ),
					'type' => 'text',
					'default' => 'Share This Event'
				),
			),

			'style_setting_sec' => array(
				array(
					'name' => 'mep_base_color',
					'label' => __( 'Base Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a Basic Color, It will chanage the icon background color, border color', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_title_bg_color',
					'label' => __( 'Label Background Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a Color Label Background', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_title_text_color',
					'label' => __( 'Label Text Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a Color Label Text', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_cart_btn_bg_color',
					'label' => __( 'Cart Button Background Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a color for Cart Button Background', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_cart_btn_text_color',
					'label' => __( 'Cart Button Text Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a color for Cart Button Text', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_calender_btn_bg_color',
					'label' => __( 'Calender Button Background Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a color for Calender Button Background', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_calender_btn_text_color',
					'label' => __( 'Calender Button Text Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a color for Calender Button Text', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_faq_title_bg_color',
					'label' => __( 'FAQ Title Background Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a color for FAQ title Background', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
				array(
					'name' => 'mep_faq_title_text_color',
					'label' => __( 'FAQ Title Text Color', 'tjoker-fbwc-integration' ),
					'desc' => __( 'Select a color for FAQ Title Text', 'tjoker-fbwc-integration' ),
					'type' => 'color',

				),
			)
		);

		return $settings_fields;
	}

	function plugin_page() {
		echo '<div class="wrap">';

		$this->settings_api->show_navigation();
		$this->settings_api->show_forms();

		echo '</div>';
	}

	/**
	 * Get all the pages
	 *
	 * @return array page names with key value pairs
	 */
	function get_pages() {
		$pages = get_pages();
		$pages_options = array();
		if ( $pages ) {
			foreach ($pages as $page) {
				$pages_options[$page->ID] = $page->post_title;
			}
		}

		return $pages_options;
	}

}
endif;