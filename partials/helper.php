<?php

	// File Security Check
	if ( ! defined( 'ABSPATH' ) ):
	    exit;
	endif;

	if( ! function_exists( 'tj_print' ) ) :
		function tj_print( $data ) {
			//if( ! WP_DEBUG ) return;
			echo '<pre>';
			if( is_array( $data ) || is_object( $data ) ) {
				print_r( $data );
			}
			else {
				echo $data;
			}
			echo '</pre>';
		}
	endif;

	if( ! function_exists( 'tj_get_option' ) ) :
		function tj_get_option( $option, $section, $default = '' ) {
			$options = get_option( $section );

			if ( isset( $options[$option] ) ) {
				return $options[$option];
			}

			return $default;
		}
	endif;

	/**
	 * Locates template.
	 *
	 * Locate the called template.
	 * Search Order:
	 * 1. /themes/theme/plugins-name/templates/$template_name
	 * 2. /plugins/plugins-name/partials/templates/$template_name.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template_name Template to load.
	 * @param string $template_path (optional) Path to templates.
	 * @param string $default_path (optional) Default path to template files.
	 * @return string Path to the template file.
	 */
	if( ! function_exists( 'tjoker_locate_template' ) ) :
		function tjoker_locate_template( $template_name, $template_path = '', $default_path = '' ) {

			// Set variable to search in templates folder of theme.
			if ( ! $template_path ) :
				$template_path = get_template_directory() . '/' . TJOKERPB_NAME . '/templates/';
			endif;
			// Set default plugin templates path.
			if ( ! $default_path ) :
				$default_path = TJOKERPB_PLUGINS_DIR . 'partials/templates/';
			endif;
			// Search template file in theme folder.
			$template = locate_template( array(
				$template_path . $template_name,
				$template_name
			) );
			// Get plugins template file.
			if ( ! $template ) :
				$template = $default_path . $template_name;
			endif;

			return apply_filters( 'tjoker_locate_template', $template, $template_name, $template_path, $default_path );
		}
	endif;


	/**
	 * Gets template.
	 *
	 * Search for the template and include the file.
	 *
	 * @since 1.0.0
	 *
	 * @see tjoker_locate_template()
	 *
	 * @param string $template_name Template to load.
	 * @param array $args Args passed for the template file.
	 * @param string $template_path (optional) Path to templates.
	 * @param string $default_path (optional) Default path to template files.
	 * @return null|void
	 */
	if( ! function_exists( 'tjoker_get_template' ) ) :
		function tjoker_get_template( $template_name, $args = array(), $tempate_path = '', $default_path = '' ) {

			if ( is_array( $args ) && isset( $args ) ) :
				extract( $args );
			endif;
			$template_file = tjoker_locate_template( $template_name, $tempate_path, $default_path );
			if ( ! file_exists( $template_file ) ) :
				_doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', $template_file ), '1.0.0' );
				return;
			endif;
			// Gets the content from the template.
			ob_start();
			require_once $template_file;
			$html = ob_get_clean();
			return $html;
		}
	endif;